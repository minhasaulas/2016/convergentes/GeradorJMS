package br.edu.up;

import java.util.Properties;
import java.util.Scanner;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class Gerador {

  public static void main(String[] args) throws NamingException, JMSException {

    String url = "http-remoting://127.0.0.1:8080";
    String ic = "org.jboss.naming.remote.client.InitialContextFactory";
    String usuario = "up";
    String senha = "positivo";
    
    Properties props = new Properties();
    props.put(Context.PROVIDER_URL, url);
    props.put(Context.INITIAL_CONTEXT_FACTORY, ic);
    props.put(Context.SECURITY_PRINCIPAL, usuario);
    props.put(Context.SECURITY_CREDENTIALS, senha);

    Context ctx = new InitialContext(props);
    
    // 1. Cria��o do contexto, busca da fila e obten��o do factory;
    //Context ctx = new InitialContext();
    Queue fila = (Queue) ctx.lookup("jms/FILA_DE_EXEMPLO");
    ConnectionFactory cf = (ConnectionFactory) ctx.lookup("jms/RemoteConnectionFactory");

    // 2. Cria��o da conex�o, da sess�o e do gerador;
    try (Connection con = cf.createConnection("up", "positivo")) {
      Session session = con.createSession(false, Session.AUTO_ACKNOWLEDGE);
      MessageProducer gerador = session.createProducer(fila);

      // 3. Cria��o e envio das TextMensages;
      Scanner scan = new Scanner(System.in);
      String msg;
      do {
        System.out.println("Digite a mensagem:");
        msg = scan.nextLine();
        TextMessage textMsg = session.createTextMessage(msg);
        gerador.send(textMsg);
      } while (!msg.equals(""));
      scan.close();
    } finally {
      ctx.close();
      System.out.println("Gerador encerrado!");
    }
  }
}